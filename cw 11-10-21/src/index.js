    /*
    Philip Sevene to Everyone (19:24)
    Создайте объект криптокошилек. В кошельке должно хранится имя владельца, несколько валют Bitcoin, 
    Ethereum, Stellar и в каждой валюте дополнительно есть имя валюты, логотип, несколько 
    монет и курс на сегодняшний день. Также в объекте кошелек есть метод при вызове 
    которого он принимает имя валюты и выводит на страницу информацию.
    "Добрый день, ... ! На вашем балансе (Название валюты и логотип) осталось N монет, 
    если вы сегодня продадите их то, получите ...грн.
    */
 const msg = prompt("Choose your coin :", "bitcoin, ethereum, stellar");

 const wallet = {
    userName: "Ivan",
    bitcoin: {
        coinName: "Bitcoin",
        coinLogo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/1.png' alt='bitcoin'>",
        quantity: 100,
        priceChange: 57741.18,

    },
    ethereum: {
        coinName: "Ethereum",
        coinLogo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png' alt='ethereum'>",
        quantity: 200,
        priceChange: 3385.76,
    },
    stellar: {
        coinName: "Stellar",
        coinLogo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/512.png' alt='stellar'>",
        quantity: 300,
        priceChange: 0.3265,
    },

    show: function (coin) {
        document.write(`Добрый день, <strong>${wallet.userName}</strong> !На вашем балансе 
        <strong>${wallet[coin].coinName}</strong> ${wallet[coin].coinLogo}осталось 
        <strong>${wallet[coin].quantity} </strong>монет, если вы сегодня продадите их то, получите   
        <strong> ${(wallet[coin].priceChange * 26 * wallet[coin].quantity).toFixed(2)}</strong> грн `);



    }
};

wallet.show(msg);